FROM golang:1.15 AS build

RUN set -x; \
    mkdir -p /go/src/goploader && cd /go/src/goploader \
    && git clone https://github.com/Depado/goploader . \
    && git checkout $GOPLOADER_VERSION \
    && go get -v ./... \
    && go get github.com/GeertJohan/go.rice@v1.0.0 \
    && go get github.com/GeertJohan/go.rice/rice@v1.0.0 \
    && rice embed-go -i=./server \
    && go build -ldflags "-linkmode external -extldflags -static" -o /goploader/server ./server

FROM scratch

ARG GOPLOADER_VERSION=6ae6953c85779de4ca39b8507fc411516263fd3a
ARG BUILD_DATE
ARG VCS_REF
ARG VCS_BRANCH

COPY --from=build /goploader/server server

LABEL \ 
	build-date=$BUILD_DATE \
	vcs-branch=$VCS_BRANCH \
	vcs-commit=$VCS_REF \
    goploader-version=$GOPLOADER_VERSION

VOLUME ["/data", "/config"]

EXPOSE 8080 8008

USER 1000

ENTRYPOINT ["/server"]

CMD ["-c", "/config/conf.yml"]